﻿using UnityEngine;
using System.Collections;

public class OpenSafeBox : MonoBehaviour
{
    public GameObject SafeBoxObject;
    public GameObject[] RegisteredPuzzleGo;
    public AudioClip DoorDrop;
    private MultiPlaceholderCheck[] RegisteredPuzzle;
    private AudioSource source;

    void Start()
    {
        RegisteredPuzzle = new MultiPlaceholderCheck[RegisteredPuzzleGo.Length];
        int i = 0;
        foreach(GameObject g in RegisteredPuzzleGo)
        {
            RegisteredPuzzle[i] = g.GetComponent<MultiPlaceholderCheck>();
            i++;
        }
        source = GetComponent<AudioSource>();
    }

    bool isDropped = false;
    void FixedUpdate()
    {
        if (RegisteredPuzzle != null)
        {
            //Check Match
            foreach (MultiPlaceholderCheck pc in RegisteredPuzzle)
            {
                if (!pc.isMatch)
                { return; }
            }

            //Set Particle
            foreach (MultiPlaceholderCheck pc in RegisteredPuzzle)
            {
                pc.isPlayParticle = true;
            }

            Debug.Log(SafeBoxObject);
            if (SafeBoxObject != null)
            { SafeBoxObject.transform.eulerAngles = new Vector3(-90,0,0);
                if (isDropped == false)
                {
                    source.PlayOneShot(DoorDrop);
                    isDropped = true;
                }
            }
        }
    }    
}