﻿using UnityEngine;
using System.Collections;

public class OpenDoorByPlate : MonoBehaviour
{
    public GameObject KeyObject;
    public GameObject PlateObject;
    public AudioClip openDoor;
   
	public float RotateIfMoreThan;
	public float RotateIfLessThan;

    static AudioSource source;
    
	void Start()
    {
        source = GetComponent<AudioSource>();
    }

    void FixedUpdate()
    {
        if (IsOpenDoor)
        {
            Debug.Log("Door Sound Played");
            if (isSoundPlayed == false)
            {
                source.PlayOneShot(openDoor);
                isSoundPlayed = true;
            }
            OpenDoor();
        }
    }
	 
    float degree = 0;bool IsOpenDoor = false;
    bool isSoundPlayed = false;
    void OpenDoor()
    {
        Debug.Log (gameObject.transform.eulerAngles.y);
		if (gameObject.transform.eulerAngles.y >= RotateIfMoreThan || gameObject.transform.eulerAngles.y <= RotateIfLessThan)
        {
            degree = -0.5f;
            gameObject.transform.eulerAngles = new Vector3(gameObject.transform.eulerAngles.x,
                                            gameObject.transform.eulerAngles.y + degree, gameObject.transform.eulerAngles.z);
        }
        else
        {
            IsOpenDoor = false;

            ColorChange cc = gameObject.GetComponentInChildren<ColorChange>();
            if (cc != null)
            { cc.StopShining = true; }

            return;
        }
    }

	public bool GetDoorStatus()
	{
		return IsOpenDoor;
	}

    void OnCollisionExit(Collision col)
    {
        if (KeyObject != null)
        {
            if (col.gameObject.name == KeyObject.name)
            {
                IsOpenDoor = true;
                PlateObject.SetActive(true);
                Destroy(KeyObject);
            }
        }
    }
}