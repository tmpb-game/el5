﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowKey : MonoBehaviour {

    public GameObject DropObject;
    public GameObject Block1;
    public GameObject Block2;
    public GameObject Block3;
    public AudioClip dropClip;

    private AudioSource source;

    // Use this for initialization
    void Start () {
        source = GetComponent<AudioSource>();
        DropObject.GetComponent<Rigidbody>().useGravity = false;
    }

    // Update is called once per frame
    bool isDropped = false;
    void FixedUpdate () {

//        Debug.Log(Block1.transform.localEulerAngles.y + " " + 
//            Block2.transform.localEulerAngles.y + " " +
//            Block3.transform.localEulerAngles.y + " ");

        if (Block1.transform.localEulerAngles.y == 180 &&
            Block2.transform.localEulerAngles.y == 180 &&
            Block3.transform.localEulerAngles.y == 180)
        {
            if (DropObject != null)
            {
				//DropObject.GetComponent<Rigidbody>().useGravity = true;
				DropObject.GetComponent<GrabPlate>().SimulateGravityFromCode = true;
				DropObject.GetComponent<GrabPlate>().EnableGrab = true;
                if (isDropped == false)
                {
                    source.PlayOneShot(dropClip);
                    isDropped = true;
                }
            }
        }
	}
}
