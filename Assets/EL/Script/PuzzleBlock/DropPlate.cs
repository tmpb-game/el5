﻿using UnityEngine;
using System.Collections;

public class DropPlate : MonoBehaviour
{
    public GameObject DropObject;
    public GameObject[] RegisteredPuzzleGo;
    public AudioClip dropClip;
    private SimplePlaceholderCheck[] RegisteredPuzzle;

    static AudioSource source;

    void Start()
    {
        source = GetComponent<AudioSource>();
        RegisteredPuzzle = new SimplePlaceholderCheck[RegisteredPuzzleGo.Length];
        int i = 0;
        foreach(GameObject g in RegisteredPuzzleGo)
        {
            RegisteredPuzzle[i] = g.GetComponent<SimplePlaceholderCheck>();
            i++;
        }
    }

    bool isDropped = false;
    void FixedUpdate()
    {
        if (RegisteredPuzzle != null)
        {
            foreach (SimplePlaceholderCheck pc in RegisteredPuzzle)
            {
                if (!pc.isMatch)
                { return; }
            }

            if (DropObject != null)
            {
				//DropObject.GetComponent<Rigidbody>().useGravity = true;
				DropObject.GetComponent<GrabPlate>().SimulateGravityFromCode = true;
				DropObject.GetComponent<GrabPlate>().EnableGrab = true;
                if (isDropped == false)
                {
                    source.PlayOneShot(dropClip);
                    isDropped = true;
                }
            }
        }
    }    
}