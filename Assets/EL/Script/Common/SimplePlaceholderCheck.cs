﻿using UnityEngine;
using System.Collections;

public class SimplePlaceholderCheck : MonoBehaviour
{
    public GameObject MatchObject;
    public AudioClip clickClip;
    public bool isMatch = false;

    private AudioSource source;

    private void Start()
    {
        source = GetComponent<AudioSource>();
    }

    void Update()
    {
    }

    void OnCollisionEnter(Collision col)
    {
        if(col.gameObject.name == MatchObject.name)
        {
            source.PlayOneShot(clickClip);
            ParticleSystem ps = gameObject.GetComponentInChildren<ParticleSystem>();
            ps.Play();
            isMatch = true;
        }
    }

    void OnCollisionExit(Collision col)
    {
        if (col.gameObject.name == MatchObject.name)
        {
            ParticleSystem ps = gameObject.GetComponentInChildren<ParticleSystem>();
            if(ps.isPlaying)
            {
                ps.Stop();
                isMatch = false;
            }
        }
    }
}