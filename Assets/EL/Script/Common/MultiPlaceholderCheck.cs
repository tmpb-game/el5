﻿using UnityEngine;
using System.Collections;

public class MultiPlaceholderCheck : MonoBehaviour
{
    public GameObject[] MatchObject;
    public bool isMatch = false;
    public bool isPlayParticle = false;

    private ParticleSystem ps;

    void Start()
    {
         ps = gameObject.GetComponentInChildren<ParticleSystem>();
    }

    void Update()
    {
        if (isPlayParticle && !ps.isPlaying)
        {
            ps.Play();
        }
        else if(!isPlayParticle && ps.isPlaying)
        {
            ps.Stop();
        }
    }

    void OnCollisionEnter(Collision col)
    {
        foreach(GameObject g in MatchObject)
        {
            if (col.gameObject.name == g.name)
            {
                isMatch = true;
                return;
            }
        }
    }

    void OnCollisionExit(Collision col)
    {
        foreach (GameObject g in MatchObject)
        {
            if (col.gameObject.name == g.name)
            {
                isMatch = false;
                return;
            }
        }
    }
}