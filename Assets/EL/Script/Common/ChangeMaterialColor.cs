﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMaterialColor : MonoBehaviour {

	private Material[] objectMat;
	private Color[] initialColor;
	private Color selectedColor;
	public bool MaterialInNotFirstChildren = false;

	// Use this for initialization
	void Start () {
		if (!MaterialInNotFirstChildren) {
			objectMat = gameObject.GetComponentInChildren<Renderer> ().materials;
		} else {
			objectMat = gameObject.GetComponent<Renderer> ().materials;
		}

		int i = 0;
		initialColor = new Color[objectMat.Length];
		foreach (Material c in objectMat) {
			initialColor[i] = c.color;
			i++;
		}

		selectedColor = new Color (240f/255f,121f/255f,2f/255f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ChangeColor()
	{
		for (int i=0;i<objectMat.Length;i++) {
			objectMat[i].color = selectedColor;
		}
	}

	public void ResetColor()
	{
		for (int i=0;i<objectMat.Length;i++) {
			objectMat[i].color = initialColor[i];
		}
	}
}
