﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class OpenLoker : MonoBehaviour {
	public GameObject[] LampuGo;
    public AudioClip openDoor, closeDoor;
	private LampCode[] LampuStatus;
	public bool ReverseCombination;
    public bool doorStatus;
	public string[] PossibleCombination;
    private AudioSource source;

	// Use this for initialization
	void Start () {

        source = GetComponent<AudioSource>();
		LampuStatus = new LampCode[LampuGo.Length];
        doorStatus = false;

		for (int i = 0; i < LampuGo.Length; i++) {
			LampuStatus[i] = LampuGo[i].GetComponent<LampCode>();
		}
	}

	void FixedUpdate () {
		string currentCombination = "";
		for (int i = 0; i < LampuStatus.Length; i++) {
			currentCombination += LampuStatus[i].IsNyala ? "1" : "0";
		}

		//Reverse
		if (ReverseCombination) {
			currentCombination = ReverseStringDirect(currentCombination);
		}

		for (int i = 0; i < PossibleCombination.Length; i++) {
			if (currentCombination == PossibleCombination[i]) {
				OpenPintuLoker ();
				return;
			}
		}

        //Default
        //if (doorStatus == true) {
        //    source.PlayOneShot(closeDoor);
        //}
        ClosePintuLoker ();
	}

	void OpenPintuLoker()
	{
        if (doorStatus == false)
        {
            source.PlayOneShot(openDoor);
            doorStatus = true;
        }
		gameObject.transform.localEulerAngles = new Vector3 (0,90,0);
	}

	void ClosePintuLoker()
	{
        if (doorStatus)
        {
            source.PlayOneShot(closeDoor);
            doorStatus = false;
        }
		gameObject.transform.localEulerAngles = new Vector3 (0,0,0);
	}

	//Faster than Array.Reverse
	public static string ReverseStringDirect(string s)
	{
		char[] array = new char[s.Length];
		int forward = 0;
		for (int i = s.Length - 1; i >= 0; i--)
		{
			array[forward++] = s[i];
		}
		return new string(array);
	}
}
