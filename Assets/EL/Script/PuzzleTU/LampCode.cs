﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Light))]
public class LampCode : MonoBehaviour {
	public bool IsNyala;
	private Light lampuIndikator;

	// Use this for initialization
	void Start () {
		lampuIndikator = gameObject.GetComponent<Light> ();	
		lampuIndikator.enabled = IsNyala;
	}
	
	// Update is called once per frame
	void Update () {

		lampuIndikator.enabled = IsNyala;
	}

	public void ToggleLampu()
	{
		IsNyala = !IsNyala;
		lampuIndikator.enabled = IsNyala;
	}
}
