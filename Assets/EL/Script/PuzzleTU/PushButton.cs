﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushButton : MonoBehaviour {
	public GameObject Lampu;
    public AudioClip clickSound;
	private LampCode lc;
	private MyoInteraction mi;
    private AudioSource source;

	public bool AllowPress { get; set; }

	// Use this for initialization
	void Start () {
		lc = Lampu.GetComponent<LampCode> ();
        source = GetComponent<AudioSource>();

        try
        {
			mi = gameObject.GetComponent<MyoInteraction>();
		}
		catch {
		}
	}

	void FixedUpdate () {
		bool PressKey = (mi != null? mi.GetMyoFist() : false) || Input.GetMouseButtonDown(0);

		if (AllowPress && PressKey) { OnPress ();
            source.PlayOneShot(clickSound);
        }
	}

	private bool IsLastPressed = false; //Pengaman agar tidak terpanggil 2x
	public void OnPress(){
		gameObject.transform.localPosition = 
			new Vector3 (0, -0.0169f, 0);
		IsLastPressed = true;
		Invoke ("OnDepress", 0.5f);
	}

	public void OnDepress(){
		gameObject.transform.localPosition = new Vector3 (0,0,0);
		if (IsLastPressed) {
			lc.ToggleLampu ();
			Debug.Log ("Toggled");
			IsLastPressed = false;
		}
	}

	float deltaAllowed = 0;
	void Update()
	{
		PeriodicAllowReset(Time.fixedDeltaTime);
	}

	public void HoldAllowReset()
	{
		deltaAllowed = 0;
	}

	public void PeriodicAllowReset(float delta)
	{
		deltaAllowed += delta;
		if (deltaAllowed > 0.1)
		{ deltaAllowed = 0; AllowPress = false; }
	}
}
