﻿using UnityEngine;
using System.Collections;
using Assets.Script.PuzzleBlock;

public class GrabObject : MonoBehaviour, IGrabable
{
    private Camera playerCamera;
    private float distance = 2.0f;
	private MyoInteraction mi;

    public bool AllowGrab { get; set; }
    public bool Grabbed { get; set; }
	public bool SimulateGravityFromCode = true;

    void Start()
    {
        playerCamera = Camera.main;

		try{
			mi = gameObject.GetComponent<MyoInteraction>();
		}
		catch {
		}
    }

	bool lastMouseClick=false;
    void FixedUpdate()
    {
		//Check Mouse Last State
		bool mouseClickState;
		if (lastMouseClick != true && Input.GetMouseButtonDown (0)) {
			mouseClickState = true;
			lastMouseClick = Input.GetMouseButtonDown (0);
			//Debug.Log ("Grab");
		} else if (!Input.GetMouseButtonDown (0)) {
			lastMouseClick = false;
			mouseClickState = false;
			//Debug.Log ("No Grab");
		} else {
			mouseClickState = false;
			//Debug.Log ("Hold Grab");
		}

        // Taruh objek depan camera
		bool GrabKey = (mi != null? mi.GetMyoFist() : false) || mouseClickState;
        if (AllowGrab && GrabKey && !Grabbed) { Grabbed = true; GrabKey = false; }
        else if (GrabKey && Grabbed) { Grabbed = false; }

		playerCamera = Camera.main;
        if (Grabbed)
        {
			if (SimulateGravityFromCode) {
				gameObject.GetComponent<Rigidbody> ().useGravity = false;
			}

            gameObject.transform.position = playerCamera.transform.position + playerCamera.transform.forward * distance;
            gameObject.transform.LookAt(playerCamera.transform.position);
            gameObject.transform.eulerAngles = new Vector3(gameObject.transform.rotation.eulerAngles.x,
                gameObject.transform.rotation.eulerAngles.y + 90,
                gameObject.transform.rotation.eulerAngles.z);
           //gameObject.transform.rotation = new Quaternion(0.0f, playerCamera.transform.rotation.y, 0.0f, playerCamera.transform.rotation.w);
        }
        else
        {
			if (SimulateGravityFromCode) {
				gameObject.GetComponent<Rigidbody> ().useGravity = true;
			}
        }

		CheckJatuh ();
    }

    float deltaAllowed = 0;
    void Update()
    {
        PeriodicAllowReset(Time.fixedDeltaTime);
    }

    public void HoldAllowReset()
    {
        deltaAllowed = 0;
    }

    public void PeriodicAllowReset(float delta)
    {
        deltaAllowed += delta;
        if (deltaAllowed > 0.5)
        { deltaAllowed = 0; AllowGrab = false; }
    }

	void CheckJatuh()
	{
		if (gameObject.transform.position.y < -5f) {
			gameObject.transform.position = new Vector3 (gameObject.transform.position.x, 0.5f,gameObject.transform.position.z);
		}
	}

}