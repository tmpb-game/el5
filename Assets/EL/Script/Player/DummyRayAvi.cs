﻿using UnityEngine;

public class DummyRayAvi : MonoBehaviour
{
	GameObject pbTemp;
    // C# example.
    void Update()
    {
        // Bit shift the index of the layer (8) to get a bit mask
        int layerMask = 1 << 8;

        // This would cast rays only against colliders in layer 8.
        // But instead we want to collide against everything except layer 8. The ~ operator does this, it inverts a bitmask.
        layerMask = ~layerMask;

        RaycastHit hit;
        // Does the ray intersect any objects excluding the player layer
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity, layerMask))
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);

            if (hit.transform.gameObject.GetComponent<GrabPlate>() != null)
            {
                hit.transform.gameObject.GetComponent<GrabPlate>().AllowGrab = true;
                hit.transform.gameObject.GetComponent<GrabPlate>().HoldAllowReset();
            }
            else if (hit.transform.gameObject.GetComponent<GrabObject>() != null)
            {
                hit.transform.gameObject.GetComponent<GrabObject>().AllowGrab = true;
                hit.transform.gameObject.GetComponent<GrabObject>().HoldAllowReset();
            }
			else if (hit.transform.gameObject.GetComponent<PushButton>() != null)
			{
				hit.transform.gameObject.GetComponent<PushButton>().AllowPress = true;
				hit.transform.gameObject.GetComponent<PushButton>().HoldAllowReset();

				if (pbTemp != hit.transform.gameObject && pbTemp != null) {
					pbTemp.GetComponent<PushButton> ().AllowPress = false;
					pbTemp = hit.transform.gameObject;
				}
			}
            else if (hit.transform.gameObject.GetComponent<PushButtonBlock>() != null)
            {
                hit.transform.gameObject.GetComponent<PushButtonBlock>().AllowPress = true;
                hit.transform.gameObject.GetComponent<PushButtonBlock>().HoldAllowReset();

                if (pbTemp != hit.transform.gameObject && pbTemp != null)
                {
                    pbTemp.GetComponent<PushButtonBlock>().AllowPress = false;
                    pbTemp = hit.transform.gameObject;
                }
            }
            //  hit.transform.SendMessage("HitByRay");
            //Debug.Log("Did Hit");
        }
        else
        {
            //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
            //Debug.Log("Did not Hit");
        }
    }
}