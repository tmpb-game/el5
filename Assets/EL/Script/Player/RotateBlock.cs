﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateBlock : MonoBehaviour {

    private MyoInteraction mi;

    public bool AllowGrab { get; set; }
    public bool Rotated { get; set; }

    void Start()
    {
        try
        {
            mi = gameObject.GetComponent<MyoInteraction>();
        }
        catch
        {
        }
    }

    void FixedUpdate()
    {
        // Taruh objek depan camera
        bool Rotate = (mi != null ? mi.GetMyoFist() : false) || Input.GetMouseButtonDown(0);

        Debug.Log("Rotate 1 : " + gameObject.transform.eulerAngles.y);
        if (Rotate)
        {
            float initialPosition = gameObject.transform.eulerAngles.y;
            OnRotate(initialPosition);
            //gameObject.transform.position = playerCamera.transform.position + playerCamera.transform.forward * distance;
            //gameObject.transform.LookAt(playerCamera.transform.position);
            //gameObject.transform.eulerAngles = new Vector3(gameObject.transform.rotation.eulerAngles.x,
            //    gameObject.transform.rotation.eulerAngles.y + 90,
            //    gameObject.transform.rotation.eulerAngles.z);
            //gameObject.transform.rotation = new Quaternion(0.0f, playerCamera.transform.rotation.y, 0.0f, playerCamera.transform.rotation.w);
        }
    }


    void OnRotate(float initialPosition)
    {
        float current = gameObject.transform.eulerAngles.y;
        if (!(current == initialPosition + 90)) {
            gameObject.transform.eulerAngles = new Vector3(gameObject.transform.eulerAngles.x,
                                            gameObject.transform.eulerAngles.y + 90, gameObject.transform.eulerAngles.z);
        }
    }

    float deltaAllowed = 0;
    void Update()
    {
        PeriodicAllowReset(Time.fixedDeltaTime);
    }

    public void HoldAllowReset()
    {
        deltaAllowed = 0;
    }

    public void PeriodicAllowReset(float delta)
    {
        deltaAllowed += delta;
        if (deltaAllowed > 0.5)
        { deltaAllowed = 0; AllowGrab = false; }
    }
}
