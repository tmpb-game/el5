﻿using UnityEngine;

public class DummyRay : MonoBehaviour
{
	GameObject pbTemp;
    // C# example.
    void Update()
    {
        // Bit shift the index of the layer (8) to get a bit mask
        int layerMask = 1 << 8;

        // This would cast rays only against colliders in layer 8.
        // But instead we want to collide against everything except layer 8. The ~ operator does this, it inverts a bitmask.
        layerMask = ~layerMask;

		RaycastSphere ();

//        RaycastHit hit;
//        // Does the ray intersect any objects excluding the player layer
//        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity, layerMask))
//        {
//            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
//
//            if (hit.transform.gameObject.GetComponent<GrabPlate>() != null)
//            {
//                hit.transform.gameObject.GetComponent<GrabPlate>().AllowGrab = true;
//                hit.transform.gameObject.GetComponent<GrabPlate>().HoldAllowReset();
//            }
//            else if (hit.transform.gameObject.GetComponent<GrabObject>() != null)
//            {
//                hit.transform.gameObject.GetComponent<GrabObject>().AllowGrab = true;
//                hit.transform.gameObject.GetComponent<GrabObject>().HoldAllowReset();
//            }
//			else if (hit.transform.gameObject.GetComponent<PushButton>() != null)
//			{
//				hit.transform.gameObject.GetComponent<PushButton>().AllowPress = true;
//				hit.transform.gameObject.GetComponent<PushButton>().HoldAllowReset();
//
//				if (pbTemp != hit.transform.gameObject && pbTemp != null) {
//					pbTemp.GetComponent<PushButton> ().AllowPress = false;
//					pbTemp = hit.transform.gameObject;
//				}
//			}
//            //  hit.transform.SendMessage("HitByRay");
//            //Debug.Log("Did Hit");
//        }
//        else
//        {
//            //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
//            //Debug.Log("Did not Hit");
//        }
    }

	private float currentDistance = 0;
	private float maxDistance = 8;
	GameObject currentGameObject;
	bool hitChangeColor = false;
	void RaycastSphere()
	{
		RaycastHit hit;
		// Does the ray intersect any objects excluding the player layer
		if (Physics.SphereCast(transform.position, 0.25f, transform.TransformDirection(Vector3.forward), out hit, maxDistance))
		{
			currentDistance = hit.distance;

			if (hit.transform.gameObject.GetComponent<GrabPlate>() != null)
			{
				hit.transform.gameObject.GetComponent<GrabPlate>().AllowGrab = true;
				hit.transform.gameObject.GetComponent<GrabPlate>().HoldAllowReset();
				currentGameObject = hit.transform.gameObject;

				if (!hit.transform.gameObject.GetComponent<GrabPlate> ().Grabbed) {
					hitChangeColor = true;
				}
			}
			else if (hit.transform.gameObject.GetComponent<GrabObject>() != null)
			{
				hit.transform.gameObject.GetComponent<GrabObject>().AllowGrab = true;
				hit.transform.gameObject.GetComponent<GrabObject>().HoldAllowReset();
				currentGameObject = hit.transform.gameObject;

				if (!hit.transform.gameObject.GetComponent<GrabObject> ().Grabbed) {
					hitChangeColor = true;
				}
			}
			else if (hit.transform.gameObject.GetComponent<PushButton>() != null)
			{
				hit.transform.gameObject.GetComponent<PushButton>().AllowPress = true;
				hit.transform.gameObject.GetComponent<PushButton>().HoldAllowReset();
				//currentGameObject = hit.transform.gameObject; //masih bug
				//hitChangeColor = true;

				if (pbTemp != hit.transform.gameObject && pbTemp != null) {
					pbTemp.GetComponent<PushButton> ().AllowPress = false;
					//pbTemp.GetComponent<ChangeMaterialColor> ().ResetColor ();
					pbTemp = hit.transform.gameObject;
				}
			}     
			else if (hit.transform.gameObject.GetComponent<PushButtonBlock>() != null)
			{
				hit.transform.gameObject.GetComponent<PushButtonBlock>().AllowPress = true;
				hit.transform.gameObject.GetComponent<PushButtonBlock>().HoldAllowReset();
				//currentGameObject = hit.transform.gameObject;
				//hitChangeColor = true;

				if (pbTemp != hit.transform.gameObject && pbTemp != null)
				{
					pbTemp.GetComponent<PushButtonBlock>().AllowPress = false;
					//pbTemp.GetComponent<ChangeMaterialColor> ().ResetColor ();
					pbTemp = hit.transform.gameObject;
				}
			}
			//  hit.transform.SendMessage("HitByRay");
			//Debug.Log("Did Hit");

			if (currentGameObject != null) {
				currentGameObject.GetComponent<ChangeMaterialColor> ().ChangeColor ();
			}
		}
		else
		{
			//Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
			//Debug.Log("Did not Hit");

		}

		if (currentGameObject != null && !hitChangeColor) {
			currentGameObject.GetComponent<ChangeMaterialColor> ().ResetColor ();
			currentGameObject = null;
		}

		hitChangeColor = false;
	}

	void OnDrawGizmos() {
		Gizmos.color = Color.red;
		Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * currentDistance, Color.yellow);
		Gizmos.DrawWireSphere(transform.position + transform.TransformDirection(Vector3.forward) * currentDistance, 0.25f);
	}

}