﻿using UnityEngine;
using System.Collections;

using LockingPolicy = Thalmic.Myo.LockingPolicy;
using Pose = Thalmic.Myo.Pose;
using UnlockType = Thalmic.Myo.UnlockType;
using VibrationType = Thalmic.Myo.VibrationType;

public class MyoBasicMoveVR : MonoBehaviour {

	// Myo game object to connect with.
	// This object must have a ThalmicMyo script attached.
	public GameObject myo = null;
	public bool IgnoreMouse = false;

	//Variables
	public float speed = 6.0F;
	public float jumpSpeed = 8.0F; 
	public float gravity = 20.0F;
	private Vector3 moveDirection = Vector3.zero;

	// The pose from the last update. This is used to determine if the pose has changed
	// so that actions are only performed upon making them rather than every frame during
	// which they are active.
	private Pose _lastPose = Pose.Unknown;

	private Transform mainCamera;
	private Vector3 dirZ;
	private Vector3 dirX;

	private Vector3 lastMouse = new Vector3(255, 255, 255); //kind of in the middle of the screen, rather than at the top (play)

	ThalmicMyo thalmicMyo;
	CharacterController controller;

	void Start(){
		mainCamera = Camera.main.transform;
		controller = GetComponent<CharacterController>();

		// Access the ThalmicMyo component attached to the Myo game object.
		thalmicMyo = myo.GetComponent<ThalmicMyo> ();

		InvokeRepeating ("Reinit", 0.5f, 0.5f);
	}

	void Reinit()
	{
		mainCamera = Camera.main.transform;
	}

	void Update() {
		if (!IgnoreMouse) {
			lastMouse = Input.mousePosition - lastMouse;
			lastMouse = new Vector3 (-lastMouse.y * 0.25f, lastMouse.x * 0.25f, 0);
			lastMouse = new Vector3 (transform.eulerAngles.x + lastMouse.x, transform.eulerAngles.y + lastMouse.y, 0);
			transform.eulerAngles = lastMouse;
			lastMouse = Input.mousePosition;
		}

		//Arah kamera di sumbu XZ
		dirZ = mainCamera.forward;
		dirX = mainCamera.right;
		dirZ.y = 0;
		dirX.y = 0;
		dirZ.Normalize ();
		dirX.Normalize ();

		// is the controller on the ground?
		if (controller.isGrounded) {

			//Input from Myo
			float horizontal = 0, vertical = 0;
			GetMyoMovement (ref horizontal, ref vertical);

			//Feed moveDirection with input.
			moveDirection = dirX*horizontal + dirZ*vertical;
			moveDirection = transform.TransformDirection(moveDirection);

			//Multiply it by speed.
			moveDirection *= speed;
			//Jumping - not impelemented with myo
			if (Input.GetButton("Jump"))
				moveDirection.y = jumpSpeed;

		}
		//Applying gravity to the controller
		moveDirection.y -= gravity * Time.deltaTime;
		//Making the character move
		controller.Move(moveDirection * Time.deltaTime);
	}

	private void GetMyoMovement(ref float horizontal, ref float vertical)
	{
		// Vibrate the Myo armband when a fist is made.
		if (thalmicMyo.pose == Pose.Fist || Input.GetKey(KeyCode.W)) {
			vertical = 1;//Forward

			ExtendUnlockAndNotifyUserAction (thalmicMyo);

			// Change material when wave in, wave out or double tap poses are made.
		}  else if (thalmicMyo.pose == Pose.FingersSpread || Input.GetKey(KeyCode.S)) {
			vertical = -1;

			ExtendUnlockAndNotifyUserAction (thalmicMyo);
		} 

		if (thalmicMyo.pose == Pose.WaveIn || Input.GetKey(KeyCode.A)) {
			horizontal = -1;

			ExtendUnlockAndNotifyUserAction (thalmicMyo);
		} else if (thalmicMyo.pose == Pose.WaveOut || Input.GetKey(KeyCode.D)) {
			horizontal = 1;

			ExtendUnlockAndNotifyUserAction (thalmicMyo);
		}
	}

	// Extend the unlock if ThalmcHub's locking policy is standard, and notifies the given myo that a user action was
	// recognized.
	void ExtendUnlockAndNotifyUserAction (ThalmicMyo myo)
	{
		ThalmicHub hub = ThalmicHub.instance;

		if (hub.lockingPolicy == LockingPolicy.Standard) {
			myo.Unlock (UnlockType.Timed);
		}

		//myo.NotifyUserAction ();
	}
}
//Debug.Log ("Hor: " + Input.GetAxis ("Horizontal"));
//Debug.Log ("Ver: " + Input.GetAxis("Vertical"));