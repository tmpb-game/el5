﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Script.PuzzleBlock
{
    public interface IGrabable
    {
        bool AllowGrab { get; set; }
        bool Grabbed { get; set; }
        void HoldAllowReset();
        void PeriodicAllowReset(float delta);
    }
}
