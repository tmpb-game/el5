﻿using UnityEngine;
using System.Collections;

public class OpenGateFinal : MonoBehaviour
{
    public GameObject KeyObject;
    public GameObject PadlockObject;
    public GameObject[] ObjectToDestroy;
    public AudioClip openDoor;
    
    private AudioSource source;

    void Start()
    {
        source = GetComponent<AudioSource>();
    }

    void FixedUpdate()
    {
    }

    void OnCollisionExit(Collision col)
    {
        if (KeyObject != null)
        {
            if (col.gameObject.name == KeyObject.name)
            {
                foreach(GameObject g in ObjectToDestroy)
                {
                    g.SetActive(false);
                }

                //source.PlayOneShot(openDoor);
                Destroy(KeyObject);
                Destroy(PadlockObject);
            }
        }
    }
}