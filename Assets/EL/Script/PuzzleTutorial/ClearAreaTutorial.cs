﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(OpenDoorByPlate))]
public class ClearAreaTutorial : MonoBehaviour {

	public GameObject[] ObjectToHide;
	public GameObject[] ObjectToShow;

	private OpenDoorByPlate finishCondition;
	// Use this for initialization
	void Start () {
		finishCondition = gameObject.GetComponent<OpenDoorByPlate> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (finishCondition.GetDoorStatus () && !isCalled) {
			ClearArea ();
			isCalled = true;
		}
	}

	bool isCalled = false;
	public void ClearArea(){
		foreach (GameObject g in ObjectToHide) {
			g.SetActive (false);
		}

		foreach (GameObject g in ObjectToShow) {
			g.SetActive (true);
		}
	}
}
